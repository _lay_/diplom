﻿using Units.LogicFreeMode;

namespace Units.Components
{
    public interface ILogicFreeModeComponent
    {
        IRoute route { get; }

        void Init(IUnit unit, string stateMachineName);
        void StartStateMachine();
        void StopStateMachine();
        void ContinueStateMachine();
        void PauseStateMachine();

        void DispatchStimul(int stimul);
        void DispatchStimul(PredefinedStateStimul stimul);
    }
}
