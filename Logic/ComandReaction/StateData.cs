﻿using System;
using System.Collections.Generic;

namespace Units.LogicFreeMode
{
    public class StateData
    {
        public IStateMachine sm { get; set; }

        private Dictionary<string, ISharedValue> values;
        
        public StateData(IStateMachine sm)
        {
            this.sm = sm;
            values = new Dictionary<string, ISharedValue>();
        }

        public bool ContainsValue(string valueName)
        {
            return values.ContainsKey(valueName);
        }

        public ISharedValue GetValue(string valueName)
        {
            return values[valueName];
        }

        public void SetValue(string valueName, Type type, object value)
        {
            values.Add(valueName, new StateDataObject(type, value));
        }
        public void SetValue(string valueName, string type, object value)
        {
            values.Add(valueName, new StateDataObject(Type.GetType(type), value));
        }

        public void ClearValues()
        {
            values.Clear();
        }
    }
}