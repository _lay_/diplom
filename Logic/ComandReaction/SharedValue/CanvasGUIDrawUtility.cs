﻿using System;
using UnityEditor;
using UnityEngine;

namespace Units.LogicFreeMode
{
    public class SharedValueGUIDrawUtility
    {
        public static object DrawValueField(string label, string n, string t, object v)
        {
            Type type = Type.GetType(t);
            bool nVal = v == null;

            if (type == typeof(bool))
            {
                bool value;
                if (nVal)
                    value = false;
                else
                    value = (bool)v;
                value = EditorGUILayout.Toggle(label, value);
                return value;
            }
            if (type == typeof(float))
            {
                float value;
                if (nVal)
                    value = 0;
                else
                {
                    if (!Single.TryParse(v.ToString(), out value))
                        value = 0;
                }
                value = EditorGUILayout.FloatField(label, value);
                return value;
            }
            if (type == typeof(GameObject))
            {
                GameObject value;
                if (nVal)
                    value = new GameObject();
                else
                    value = (GameObject)v;
                value = (GameObject)EditorGUILayout.ObjectField(label, value, type, true);
                return value;
            }
            if (type == typeof(int))
            {
                int value;
                if (nVal)
                    value = 0;
                else
                    value = (int)v;
                value = EditorGUILayout.IntField(label, value);
                return value;
            }
            if (type == typeof(string))
            {
                string value;
                if (nVal)
                    value = "";
                else
                    value = (string)v;
                value = EditorGUILayout.TextField(label, value);
                return value;
            }
            if (type == typeof(Transform))
            {
                Transform value;
                if (nVal)
                    value = new GameObject().transform;
                else
                    value = (Transform)v;
                value = (Transform)EditorGUILayout.ObjectField(label, value, type, true);
                return value;
            }
            if (type == typeof(Vector2))
            {
                Vector2 value;
                if (nVal)
                    value = new Vector2(0, 0);
                else
                    value = (Vector2)v;
                value = EditorGUILayout.Vector2Field(label, value);
                return value;
            }
            if (type == typeof(Vector3))
            {
                Vector3 value;
                if (nVal)
                    value = new Vector3(0, 0, 0);
                else
                    value = (Vector3)v;
                value = EditorGUILayout.Vector3Field(label, value);
                return value;
            }
            if (type == typeof(Vector4))
            {
                Vector4 value;
                if (nVal)
                    value = new Vector4(0, 0, 0, 0);
                else
                    value = (Vector4)v;
                value = EditorGUILayout.Vector4Field(label, value);
                return value;
            }

            return v;
        }
    }
}
