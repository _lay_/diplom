﻿using UnityEngine;
using System;

namespace Units.LogicFreeMode
{
    [Serializable]
    public class SharedVector3 : SharedValue<Vector3>
    {
        public static implicit operator SharedVector3(Vector3 value) { return new SharedVector3 { value = value }; }
    }
}
