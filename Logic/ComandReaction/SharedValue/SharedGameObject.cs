﻿using UnityEngine;
using System;

namespace Units.LogicFreeMode
{
    [Serializable]
    public class SharedGameObject : SharedValue<GameObject>
    {
        public static implicit operator SharedGameObject(GameObject value) { return new SharedGameObject { value = value }; }
    }
}
