﻿using UnityEngine;
using System;

namespace Units.LogicFreeMode
{
    [Serializable]
    public class SharedTransform : SharedValue<Transform>
    {
        public static implicit operator SharedTransform(Transform value) { return new SharedTransform { value = value }; }
    }
}
