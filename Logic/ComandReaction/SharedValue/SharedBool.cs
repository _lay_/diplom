﻿using System;

namespace Units.LogicFreeMode
{
    [Serializable]
    public class SharedBool : SharedValue<bool>
    {
        public static implicit operator SharedBool(bool value) { return new SharedBool { value = value }; }
    }
}
