﻿using System;

namespace Units.LogicFreeMode
{
    [Serializable]
    public class SharedString : SharedValue<string>
    {
        public static implicit operator SharedString(string value) { return new SharedString { value = value }; }
    }
}
