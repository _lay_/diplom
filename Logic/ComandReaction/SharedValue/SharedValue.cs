﻿using System;

namespace Units.LogicFreeMode
{
    public interface ISharedValue
    {
        object GetObject();
        void SetObject(object value);
        Type GetValueType();
    }

    public interface ISharedValue<T> : ISharedValue
    {
        T GetValue();
        void SetValue(T value);
    }

    [Serializable]
    public class StateDataObject : ISharedValue
    {
        protected Type type { get; set; }
        protected object value { get; set; }

        public StateDataObject(Type type, object value)
        {
            this.type = type;
            this.value = value;
        }

        public object GetObject()
        {
            return value;
        }

        public void SetObject(object value)
        {
            this.value = value;
        }     

        public Type GetValueType()
        {
            return type;
        }
    }

    [Serializable]
    public abstract class SharedValue<T> : ISharedValue<T>
    {
        protected T value { get; set; }

        public T GetValue()
        {
            return value;
        }

        public void SetValue(T value)
        {
            this.value = value;
        }

        public object GetObject()
        {
            return value;
        }

        public void SetObject(object value)
        {
            this.value = (T)value;
        }

        public Type GetValueType()
        {
            return typeof(T);
        }
    }
}
