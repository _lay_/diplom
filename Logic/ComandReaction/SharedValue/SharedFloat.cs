﻿using System;

namespace Units.LogicFreeMode
{
    [Serializable]
    public class SharedFloat : SharedValue<float>
    {
        public static implicit operator SharedFloat(float value) { return new SharedFloat { value = value }; }
    }
}
