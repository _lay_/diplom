﻿using UnityEngine;
using System;

namespace Units.LogicFreeMode
{
    [Serializable]
    public class SharedVector4 : SharedValue<Vector4>
    {
        public static implicit operator SharedVector4(Vector4 value) { return new SharedVector4 { value = value }; }
    }
}
