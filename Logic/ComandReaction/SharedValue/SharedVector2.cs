﻿using UnityEngine;
using System;

namespace Units.LogicFreeMode
{
    [Serializable]
    public class SharedVector2 : SharedValue<Vector2>
    {
        public static implicit operator SharedVector2(Vector2 value) { return new SharedVector2 { value = value }; }
    }
}
