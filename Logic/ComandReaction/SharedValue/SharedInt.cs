﻿using System;

namespace Units.LogicFreeMode
{
    [Serializable]
    public class SharedInt : SharedValue<int>
    {
        public static implicit operator SharedInt(int value) { return new SharedInt { value = value }; }
    }
}
