﻿using UnityEngine;
using System;

namespace Units.LogicFreeMode
{
    [Serializable]
    public class SharedObject : SharedValue<UnityEngine.Object>
    {
        public static implicit operator SharedObject(UnityEngine.Object value) { return new SharedObject { value = value }; }
    }
}
