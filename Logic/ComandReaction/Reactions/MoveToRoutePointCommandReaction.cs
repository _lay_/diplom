﻿using System;
using System.Collections.Generic;
using Tiles;

namespace Units.LogicFreeMode
{
    public class MoveToRoutePointCommandReaction : MoveBaseCommandReaction
    {
        [Inject]
        public ITileMap TileMap { get; protected set; }

        private IRoute route { get { return unit.LogicFreeModeComponent.route; } }

        protected override CellCoordinates GetDestination()
        {
            if (CellReachable(route.current))
            {
                return route.current;
            }
            else
            {
                return GetNearReachableCell(route.current);
            }
        }

        private bool CellReachable(CellCoordinates cell)
        {
            WalkableTileType wt = TileMap.GetCellWalkableType(cell);

            return wt == WalkableTileType.Ground;
        }

        private CellCoordinates GetNearReachableCell(CellCoordinates needCell)
        {
            List<CellCoordinates> nearCells = new List<CellCoordinates>();

            int distance = 3;

            TileMap.GetCellsByRadius(route.current, distance, nearCells);

            SortCells(ref nearCells, route.current, distance);

            foreach (CellCoordinates newCell in nearCells)
            {
                if (CellReachable(newCell))
                    return newCell;
            }

            LogMessage(String.Format("Unit {0} cell {1} not reachable!", unit.ID, route.current));
            return route.current;
        }

        // Need optimization.
        private void SortCells(ref List<CellCoordinates> cells, CellCoordinates center, int distance)
        {
            int debugIf = 0, debugSwap = 0;

            int j = cells.Count - 1;

            for (int n = distance; n > 0; n--)
            {
                for (int i = 0; i < cells.Count && j >= 0; i++)
                {
                    int max = cells[i].DistanceTo(center);

                    debugIf++;

                    if (max == n)
                    {
                        debugSwap++;

                        CellCoordinates buf = cells[i];
                        cells[i] = cells[j];
                        cells[j] = buf;

                        j--;
                    }
                }
            }

            //LogMessage(String.Format("Sort nearCells statistic: ifs={0}, swaps={1}", debugIf, debugSwap));
        }
    }
}
