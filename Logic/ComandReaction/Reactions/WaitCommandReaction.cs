﻿using System.Collections;
using UnityEngine;

namespace Units.LogicFreeMode
{
    class WaitCommandReaction : CommandReaction
    {
        private SharedFloat time { get; set; }

        float _time = 0;

        protected override void Run()
        {
            _time = time.GetValue();

            Retain();
            StartCoroutine();
        }

        protected override IEnumerator Coroutine()
        {
            yield return new WaitForSeconds(_time);

            DispatchStimul(PredefinedStateStimul.WaitCompleteStimul);
            Release();
        }
    }
}
