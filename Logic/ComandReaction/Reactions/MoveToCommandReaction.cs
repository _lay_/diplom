﻿using Tiles;
using UnityEngine;

namespace Units.LogicFreeMode
{
    public class MoveToCommandReaction : MoveBaseCommandReaction
    {
        private SharedFloat x { get; set; }
        private SharedFloat z { get; set; }
        
        protected override CellCoordinates GetDestination()
        {
            Vector3 destination = new Vector3(x.GetValue(), 0, z.GetValue());
            return new CellCoordinates((int)destination.x, (int)destination.z);
        }
    }
}
