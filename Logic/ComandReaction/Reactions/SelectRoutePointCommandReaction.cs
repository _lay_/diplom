﻿
namespace Units.LogicFreeMode
{
    public class SelectRoutePointCommandReaction : CommandReaction
    {
        private SharedBool routeLoop { get; set; }

        private IRoute route { get { return unit.LogicFreeModeComponent.route; } }

        protected override void Run()
        {
            if (route.isLast)
            {
                if (routeLoop.GetValue())
                    route.GoFirst();
                DispatchStimul(PredefinedStateStimul.RouteEndStimul);
            }
            else
            {
                route.GoNext();
            }
        }
    }
}
