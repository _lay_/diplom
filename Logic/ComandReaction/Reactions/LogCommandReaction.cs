﻿
namespace Units.LogicFreeMode
{
    public class LogCommandReaction : CommandReaction
    {
        public SharedString logString { get; set; }

        protected override void Run()
        {
            LogMessage(logString.GetValue());
        }
    }
}
