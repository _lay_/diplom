﻿
namespace Units.LogicFreeMode
{
    class DispatchStimulCommandReaction : CommandReaction
    {
        public SharedString stimul { get; set; }

        protected override void Run()
        {
            DispatchStimul(stimul.GetValue());
        }
    }
}
