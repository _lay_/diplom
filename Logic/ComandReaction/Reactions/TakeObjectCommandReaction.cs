﻿using UnityEngine;

namespace Units.LogicFreeMode
{
    public class TakeObjectCommandReaction : CommandReaction
    {
        private SharedString objectName { get; set; }

        protected override void Run()
        {
            DispatchStimul(PredefinedStateStimul.ObjectTakedStimul);
            //controller.TakeObject(objectName.GetValue());
        }
    }
}

