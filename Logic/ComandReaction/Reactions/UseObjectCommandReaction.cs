﻿
namespace Units.LogicFreeMode
{
    public class UseObjectCommandReaction : CommandReaction
    {
        private SharedString objectName { get; set; }

        protected override void Run()
        {
            DispatchStimul(PredefinedStateStimul.ObjectUsedStimul);
            //controller.UseObject(objectName.GetValue());
        }
    }
}

