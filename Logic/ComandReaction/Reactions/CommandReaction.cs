﻿using Core;
using Core.Unity;
using strange.extensions.command.impl;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using UnityEngine;

namespace Units.LogicFreeMode
{
    public abstract class CommandReaction : Command
    {
        [InjectValue]
        public StateData stateData { get; protected set; }

        [Inject]
        public ICoroutineCreator coroutineCreator { get; protected set; }
        [Inject]
        public IStateStimulDatabase stateStimulDatabase { get; protected set; }

        private Coroutine coroutine = null;

        protected IStateMachine sm { get { return stateData.sm; } }
        protected IUnit unit { get { return sm.unit; } }

        public override void Execute()
        {
            SetSharedValues();
            Run();
        }

        /// <summary>
        /// Main method for reaction logic
        /// </summary>
        protected abstract void Run();

        #region SharedValuesSetup

        private List<Type> FindSharedTypes()
        {
            Type derivedType = typeof(ISharedValue);
            Type commandType = this.GetType();
            Assembly assembly = Assembly.GetAssembly(commandType);
            List<Type> types = assembly
                        .GetTypes()
                        .Where(t =>
                                t != derivedType &&
                                derivedType.IsAssignableFrom(t)
                            ).ToList();

            return types;
        }

        private void SetSharedValues()
        {
            List<Type> sharedValueTypes = FindSharedTypes();

            var properties = this.GetType().GetProperties(
                    BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance
                ).Where(n => sharedValueTypes.Contains(n.PropertyType));

            foreach (var property in properties)
            {
                string name = property.Name;
                if (stateData.ContainsValue(name))
                {
                    // Get value from data
                    ISharedValue dataObj = stateData.GetValue(name);
                    Type dataType = dataObj.GetValueType();
                    object dataValue = dataObj.GetObject();

                    // Init sharedValue
                    Type type = property.PropertyType;

                    if (property.GetValue(this, null) == null)
                    {
                        property.SetValue(this, Activator.CreateInstance(type));
                    }

                    // Set value
                    MethodInfo seter = type.GetMethod("SetValue");

                    seter.Invoke(property.GetValue(this, null), new object[] { dataValue });
                }
            }
        }

        #endregion

        #region coroutine

        protected void StartCoroutine()
        {
            LogMessage("CoStart");
            StopCoroutine();
            coroutine = coroutineCreator.StartCoroutine(Coroutine());
        }

        protected void StopCoroutine()
        {
            if (coroutine != null)
            {
                LogMessage("CoStop");
                coroutineCreator.StopCoroutine(coroutine);
                coroutine = null;
            }
        }

        protected virtual IEnumerator Coroutine()
        {
            return null;
        }

        #endregion

        private new void Cancel()
        {
            LogMessage("Cancel");
            StopCoroutine();
            base.Cancel();
        }

        protected void DispatchStimul(int stimul)
        {
            sm.DispatchStimul(stimul);
        }
        protected void DispatchStimul(string stimul)
        {
            sm.DispatchStimul(stateStimulDatabase.GetId(stimul));
        }
        protected void DispatchStimul(PredefinedStateStimul stimul)
        {
            sm.DispatchStimul(stimul);
        }

        protected void LogMessage(string messageString = "")
        {
            Debug.Log(String.Format("Unit {0}: {1}", unit.ID, messageString));
        }
    }
}
