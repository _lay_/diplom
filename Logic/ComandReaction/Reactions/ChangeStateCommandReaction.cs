﻿
namespace Units.LogicFreeMode
{
    public class ChangeStateCommandReaction : CommandReaction
    {
        private SharedInt newStateId { get; set; }

        protected override void Run()
        {
            sm.ChangeState(newStateId.GetValue());
        }
    }
}
