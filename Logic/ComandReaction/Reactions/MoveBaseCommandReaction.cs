﻿using Managers.Interfaces;
using System;
using System.Collections;
using Tiles;

namespace Units.LogicFreeMode
{
    public abstract class MoveBaseCommandReaction : CommandReaction
    {
        [Inject]
        public IGroupUnitsManager GroupUnitsManager { get; protected set; }

        private CellCoordinates cellDestination = CellCoordinates.zero;


        protected override void Run()
        {
            if (GroupUnitsManager.GetUnitByCellCoordinates(cellDestination) != null)
            {
                LogMessage("Destination is busy");
                DispatchStimul(PredefinedStateStimul.WalkCompleteStimul);
                return;
            }

            cellDestination = GetDestination();

            Retain();
            StartCoroutine();
        }

        protected override IEnumerator Coroutine()
        {
            LogMessage("Wait motionComponentEnabled");
            do{
                yield return null;
            } while (!unit.MotionComponent.Enabled);
            LogMessage("Wait motionComponentEnabled complete");
            SetDestination();
        }

        private void SetDestination()
        {
            unit.MotionComponent.SetDestination(cellDestination);
            LogMessage(String.Format("Move to ({0}; 0; {1})", cellDestination.X, cellDestination.Y));
            Release();
        }

        protected abstract CellCoordinates GetDestination();
    }
}
