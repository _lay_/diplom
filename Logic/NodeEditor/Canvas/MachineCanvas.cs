﻿using NodeEditorFramework;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace Units.LogicFreeMode
{
    [NodeCanvasType("MachineCanvas")]
    public class MachineCanvas : NodeCanvas
    {
        public override bool allowRecursion { get { return false; } }
        public override string canvasName { get { return "Machine Canvas"; } }

        public IStateStimulDatabase stateStimulDatabase { get { return AssetDatabase.LoadAssetAtPath<StateStimulDatabase>(stimulDatabasePath); } }
        private const string stimulDatabasePath = "Assets/Resources/Data/LogicComponentConfig/StateStimulDatabase.asset";

        public string jsonFilePath = "";

        public const string stateNodeID = "StateNode";
        public const string rootNodeID = "StateRootNode";
        public const string actionNodeId = "ActionNode";
        public const string concentratorNodeId = "ConcentratorNode";

        public int statesCount { get { return GetStatesCount(); } }

        protected StateRootNode rootNode { get; set; }


        protected override void OnCreate()
        {
            base.OnCreate();
            ValidateSelf();
        }

        bool callbacksOn = false;

        private void RegisterCallbacks()
        {
            if (callbacksOn)
                return;
            //Debug.Log("Register");
            NodeEditorCallbacks.OnAddConnection += OnAddConnection;
            NodeEditorCallbacks.OnRemoveConnection += OnRemoveConnection;
            NodeEditorCallbacks.OnDeleteNode += OnDeleteNode;
            NodeEditorCallbacks.OnAddNode += OnAddOrMoveNode;
            NodeEditorCallbacks.OnMoveNode += OnAddOrMoveNode;
            callbacksOn = true;
        }

        private void UnRegisterCallbacks()
        {
            if (!callbacksOn)
                return;
            //Debug.Log("UnRegister");
            NodeEditorCallbacks.OnAddConnection -= OnAddConnection;
            NodeEditorCallbacks.OnRemoveConnection -= OnRemoveConnection;
            NodeEditorCallbacks.OnDeleteNode -= OnDeleteNode;
            NodeEditorCallbacks.OnAddNode -= OnAddOrMoveNode;
            NodeEditorCallbacks.OnMoveNode -= OnAddOrMoveNode;
            callbacksOn = false;
        }

        private void OnAddConnection(ConnectionPort port, ConnectionPort connection)
        {
            if (port.body.GetID != "ConcentratorNode")
            {
                return;
            }

            if (port.styleID != connection.styleID)
            {
                port.ClearConnections();
                Debug.Log("This connect do not allow!");
            }
            SaveToJson();
        }

        private void OnRemoveConnection(ConnectionPort port, ConnectionPort connection)
        {
            SaveToJson();
        }

        private void OnDeleteNode(Node node)
        {
            if (node.GetID == stateNodeID)
            {
                int deleteId = ((StateNode)node).stateId;

                for (int i = deleteId+1; i < statesCount; i++)
                {
                    StateNode state = GetState(i);
                    state.stateId = deleteId;
                    deleteId++;
                }
            }

            SaveToJson();
        }

        private void OnAddOrMoveNode(Node node)
        {
            SaveToJson();
        }

        public int GetStatesCount()
        {
            int statesCount = 0;
            foreach (Node node in nodes)
            {
                if (node.GetID == stateNodeID)
                {
                    statesCount++;
                }
            }
            return statesCount;
        }

        public StateNode GetState(int stateId)
        {
            StateNode node = (StateNode)nodes.Find(x => x.GetID == stateNodeID && ((StateNode)x).stateId == stateId);

            return node;
        }

        public StateNode[] GetStates()
        {
            List<StateNode> res = new List<StateNode>();

            foreach (Node node in nodes)
            {
                if (node.GetID == stateNodeID)
                    res.Add((StateNode)node);
            }

            return res.ToArray();
        }

        public StateRootNode GetStateRootNode()
        {
            return rootNode;
        }

        protected override void ValidateSelf()
        {
            if (rootNode == null)
            {
                rootNode = nodes.Find((Node n) => n.GetID == rootNodeID) as StateRootNode;
                if (rootNode == null)
                    CreateRootNode();
            }
        }

        public void CrateNodesFromJson()
        {
            UnRegisterCallbacks();

            StateMachineItem machine = LoadFromJson();

            // concentrator connections after build nodes (<stateId, prevConnections>)
            var buf = new Dictionary<int, List<ConnectionKnob>>(new IntSpecialComparer());

            //root
            this.nodes.Clear();
            if (nodes.Find(x => x.GetID == rootNodeID) == null)
                CreateRootNode();

            // states
            foreach (var state in machine.states)
            {
                ConnectionKnob prev = null;
                if (state.id == 0)
                    prev = rootNode.startState;

                StateNode snode = CreateStateNode(state.id, state.pos, prev);

                // actions
                foreach (var stimul in state.reactions)
                {
                    string stimulName = stimul.stimul;

                    for (int i = 0; i < stimul.reactions.Length; i++)
                    {
                        ReactionItem reaction = stimul.reactions[i];

                        if (i == 0)
                            prev = snode.GetStimulPort(stimulName);

                        if (reaction.reactionType == typeof(ChangeStateCommandReaction).ToString())
                        {   // changeState reaction (last in collection)
                            int stateId = (int)reaction.parameters[0].value;
                            if (!buf.ContainsKey(stateId))
                            {
                                buf.Add(stateId, new List<ConnectionKnob>());
                            }
                            buf[stateId].Add(prev);
                        }
                        else
                        {   // other reaction
                            ActionNode anode = CreateActionNode(
                                reaction.pos, prev,
                                reaction.reactionType, reaction.parameters
                                );
                            
                            prev = anode.nextKnob;
                        }
                    }                         
                }
            }

            // state.prev connections
            foreach (var item in buf)
            {
                StateNode state = GetState(item.Key);
                Vector2 pos = state.position;
                var conns = item.Value;

                if (conns.Count > 1)
                {   // connection with concentrator node                  
                    for (int i=0; i<conns.Count; i += 3)
                    {
                        pos.x -= 150;
                        ConcentratorNode cnode = CreateConcentratorNode(pos, state.prevState);
                        if (i < conns.Count)
                            cnode.prevT.ApplyConnection(conns[i]);
                        if (i+1 < conns.Count)
                            cnode.prevL.ApplyConnection(conns[i+1]);
                        if (i+2 < conns.Count)
                            cnode.prevB.ApplyConnection(conns[i+2]);
                    }
                }
                else
                {   // connection without concentrator node
                    state.prevState.ApplyConnection(conns[0]);
                }
            }

            RegisterCallbacks();
        }

        private void CreateRootNode()
        {
            rootNode = Node.Create(rootNodeID, Vector2.zero, null, true) as StateRootNode;
        }

        private StateNode CreateStateNode(int stateId, Vector2 pos, ConnectionKnob prev)
        {
            StateNode node = Node.Create(stateNodeID, pos, null, true) as StateNode;

            if (prev != null)
                node.prevState.ApplyConnection(prev);

            node.stateId = stateId;

            return node;
        }

        private ActionNode CreateActionNode(Vector2 pos, ConnectionKnob prev, string reactionType, ParameterItem[] parameters)
        {
            ActionNode node = Node.Create(actionNodeId, pos, null, true) as ActionNode;

            node.SetReactionData(reactionType, parameters);

            if (prev != null)
                node.prevKnob.ApplyConnection(prev);

            return node;
        }

        private ConcentratorNode CreateConcentratorNode(Vector2 pos, ConnectionKnob next)
        {
            ConcentratorNode node = Node.Create(concentratorNodeId, pos, null, true) as ConcentratorNode;

            if (next != null)
                node.next.ApplyConnection(next);

            return node;
        }

        public override bool CanAddNode(string nodeID)
        {
            //Debug.Log ("Check can add node " + nodeID);
            if (nodeID == rootNodeID)
                return !nodes.Exists((Node n) => n.GetID == rootNodeID);
            return true;
        }

        JsonStateDataProvider dataProvider = new JsonStateDataProvider();

        public void SaveToJson()
        {
            //Debug.Log("Save to json");
            StateMachineItem machine = StateNodesReader.ReadStateCanvas(this);

            dataProvider.Save(machine, jsonFilePath);
        }

        private StateMachineItem LoadFromJson()
        {
            //Debug.Log("Load from json");
            StateMachineItem smItem = dataProvider.Load(jsonFilePath);

            return smItem;
        }
    }
}
