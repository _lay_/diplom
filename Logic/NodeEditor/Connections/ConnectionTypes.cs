﻿using NodeEditorFramework;
using UnityEngine;

namespace Units.LogicFreeMode
{
    public class StateConnectionType : ConnectionKnobStyle //: IConnectionTypeDeclaration
    {
        public override string Identifier { get { return "StateConnection"; } }
        public override Color Color { get { return Color.yellow; } }
    }

    public class StateStimulConnectionType : ConnectionKnobStyle // : IConnectionTypeDeclaration
    {
        public override string Identifier { get { return "StateStimulConnection"; } }
        public override Color Color { get { return Color.cyan; } }
    }
}
