﻿using NodeEditorFramework;
using System;
using UnityEngine;

namespace Units.LogicFreeMode
{
    [Node(true, "Machine/State Base Node", new Type[] { typeof(MachineCanvas) })]
    public abstract class StateBaseNode : Node
    {
        public abstract Type GetObjectType { get; }

        public override string Title { get { return "State Base Node"; } }
        public override Vector2 MinSize { get { return new Vector2(250, 150); } }
        public override bool AutoLayout { get { return true; } }  // resizable

        protected MachineCanvas machineCanvas { get { return (MachineCanvas)canvas; } }

        protected abstract void DrawGUI();

        public override void NodeGUI()
        {
            DrawGUI();
        }
    }
}
