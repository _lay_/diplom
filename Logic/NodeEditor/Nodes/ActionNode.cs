﻿using NodeEditorFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using UnityEditor;
using UnityEngine;

namespace Units.LogicFreeMode
{
    [Node(false, "Machine/Action Node", new Type[] { typeof(MachineCanvas) })]
    public class ActionNode : StateBaseNode
    {
        public override string Title { get { return "Action Node"; } }
        public override Vector2 MinSize { get { return new Vector2(240, 40); } }
        public override bool AutoLayout { get { return true; } }

        private const string id = "ActionNode";
        public override string GetID { get { return id; } }
        public override Type GetObjectType { get { return typeof(ActionNode); } }

        [ConnectionKnob("", Direction.In, "StateStimulConnection", NodeSide.Left, 50, MaxConnectionCount = ConnectionCount.Single)]
        public ConnectionKnob prevKnob;
        [ConnectionKnob("", Direction.Out, "StateStimulConnection", NodeSide.Right, 50, MaxConnectionCount = ConnectionCount.Single)]
        public ConnectionKnob nextKnob;

        [SerializeField]
        private List<ParameterItem> parameters;

        [SerializeField]
        private int choisenTypeI;

        [SerializeField]
        private string actionTypeName;

        private Type actionType
        {
            get { if (String.IsNullOrEmpty(actionTypeName)) return null; else return Type.GetType(actionTypeName); }
            set { actionTypeName = value.ToString(); }
        }

        #region derivedTypesFind
        // Find ActionType for node (without ChangeState)

        private static List<Type> commandTypes = FindAllDerivedTypes<CommandReaction>();
        // need get derivedTypes for SharedValue<T>!!!
        private static List<Type> sharedValueTypes = FindAllDerivedTypes<ISharedValue>();

        private static List<Type> FindAllDerivedTypes<T>()
        {
            return FindAllDerivedTypes<T>(Assembly.GetAssembly(typeof(T)));
        }

        private static List<Type> FindAllDerivedTypes<T>(Assembly assembly)
        {
            var derivedType = typeof(T);
            var types = assembly
                        .GetTypes()
                        .Where(t =>
                                t != derivedType &&
                                derivedType.IsAssignableFrom(t)
                            ).ToList();

            // types not for use
            types.Remove(typeof(ChangeStateCommandReaction));
            types.Remove(typeof(MoveBaseCommandReaction));

            return types;
        }

        #endregion

        protected override void OnCreate()
        {
            parameters = new List<ParameterItem>();

            choisenTypeI = commandTypes.IndexOf(actionType);
            if (choisenTypeI == -1)
            {
                choisenTypeI = 0;
            }
            actionType = commandTypes[choisenTypeI];

            SetParametersFromType();
        }

        bool allowLoad = true;

        protected override void DrawGUI()
        {
            if (parameters == null)
            {
                parameters = new List<ParameterItem>();
                if (allowLoad)
                    machineCanvas.CrateNodesFromJson();
                allowLoad = false;
            }

            EditorGUILayout.BeginVertical("Box");

            GUILayout.BeginHorizontal();
            prevKnob.DisplayLayout();
            nextKnob.DisplayLayout();
            GUILayout.EndHorizontal();

            // ActionType
            string[] buf = new string[commandTypes.Count];
            for (int i = 0; i < commandTypes.Count; i++)
            {
                buf[i] = commandTypes[i].ToString().Split('.').Last().Replace("CommandReaction", "");
            }
            int tmp = choisenTypeI;
            choisenTypeI = EditorGUILayout.Popup("action type", choisenTypeI, buf);

            // parameters and type init
            if (tmp != choisenTypeI)
            {
                actionType = commandTypes[choisenTypeI];

                if (parameters.Count > 0)
                {
                    ClearParameters();
                }
                SetParametersFromType();
            }

            GUILayout.EndVertical();

            if (parameters.Count > 0)
            {
                GUILayout.BeginVertical();
                GUILayout.ExpandWidth(false);

                GUILayout.BeginHorizontal();
                GUILayout.Label("Parameters");

                GUILayout.EndHorizontal();
                GUILayout.Space(5);

                DrawParameters();

                GUILayout.ExpandWidth(false);
                GUILayout.EndVertical();
            }
        }

        private void DrawParameters()
        {
            EditorGUILayout.BeginVertical();
            int i = 0;

            foreach (var parameter in parameters)
            {
                GUILayout.BeginVertical();
                GUILayout.BeginHorizontal();
                EditorGUILayout.LabelField(i + ".", GUILayout.MaxWidth(15));
                GUILayout.BeginVertical();

                string label = String.Format("{0} ({1})", parameter.name, parameter.type.Split('.').Last());

                object tmp = parameter.value;
                parameter.value = SharedValueGUIDrawUtility.DrawValueField(label, parameter.name, parameter.type, parameter.value);
                if (!parameter.value.Equals(tmp))
                {
                    ((MachineCanvas)canvas).SaveToJson();
                }

                GUILayout.EndVertical();
                GUILayout.EndHorizontal();
                GUILayout.EndVertical();
            }
            GUILayout.EndVertical();
        }

        private void ClearParameters()
        {
            parameters.Clear();
        }

        private int GetParamCount()
        {
            return actionType.GetProperties(
                    BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance
                ).Where(n => sharedValueTypes.Contains(n.PropertyType)).Count();
        }

        private void SetParametersFromType()
        {
            var properties = actionType.GetProperties(
                    BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance
                ).Where(n => sharedValueTypes.Contains(n.PropertyType));

            int count = properties.Count();

            for (int i = 0; i < count; i++)
            {
                PropertyInfo property = properties.ElementAt(i);
                Type type = property.PropertyType;
                Type typeValue = type.BaseType.GenericTypeArguments[0];
                ConstructorInfo constructor = property.PropertyType.GetConstructor(new Type[] {  });

                parameters.Add(new ParameterItem(property.Name, typeValue.ToString(), null));
            }
        }

        public ParameterItem[] GetAllParameters()
        {
            return parameters.ToArray();
        }

        private StateBaseNode GetActionNext()
        {
            if (nextKnob.connected())
            {
                Node node = nextKnob.connections[0].body;
                if (node != null && node.GetID == "ConcentratorNode")
                    node = ((ConcentratorNode)node).GetNextNode();

                return (StateBaseNode)node;
            }
            else
                return null;
        }

        public bool ActionToAction()
        {
            StateBaseNode node = GetActionNext();

            return node != null && node.GetID == "ActionNode";
        }

        public bool ActionToState()
        {
            StateBaseNode node = GetActionNext();

            return node != null && node.GetID == "StateNode";
        }

        public ActionNode GetNextAction()
        {
            return (ActionNode)GetActionNext();
        }

        public StateNode GetNextState()
        {
            return (StateNode)GetActionNext();
        }

        public Type GetActionType()
        {
            return actionType;
        }

        public void SetReactionData(string reactionTypeName, ParameterItem[] prs)
        {
            Type type = Type.GetType(reactionTypeName);
            choisenTypeI = commandTypes.IndexOf(type);
            actionType = type;

            ClearParameters();
            SetParametersFromType();

            foreach (var p in prs)
            {
                int i =  parameters.FindIndex(x => x.name == p.name && x.type == p.type);
                if (i > -1)
                {
                    parameters[i].value = p.value;
                }
            }
        }
    }
}
