﻿using NodeEditorFramework;
using System;
using UnityEditor;
using UnityEngine;

namespace Units.LogicFreeMode
{
    [Node(false, "Machine/State Root Node", new Type[] { typeof(MachineCanvas) })]
    public class StateRootNode : StateBaseNode
    {
        public override string Title { get { return "Machine Root Node"; } }
        public override Vector2 MinSize { get { return new Vector2(120, 40); } }

        private const string id = "StateRootNode";
        public override string GetID { get { return id; } }
        public override Type GetObjectType { get { return typeof(StateRootNode); } }


        [ConnectionKnob("To Start", Direction.Out, "StateConnection", NodeSide.Right)]
        public ConnectionKnob startState;


        protected override void DrawGUI()
        {
            EditorGUILayout.BeginVertical("Box");
            GUILayout.BeginHorizontal();
            EditorGUIUtility.labelWidth = 70;
            startState.DisplayLayout();
            GUILayout.EndHorizontal();
            GUILayout.EndVertical();
        }

        public StateNode GetFirstState()
        {
            if (startState.connected())
                return null;

            return (StateNode)startState.connections[0].body;
        }
    }
}
