﻿using NodeEditorFramework;
using System;
using UnityEngine;

namespace Units.LogicFreeMode
{
    [Node(false, "Machine/Concentrator Node", new Type[] { typeof(MachineCanvas) })]
    public class ConcentratorNode : StateBaseNode
    {
        public override string Title { get { return ""; } }
        public override Vector2 MinSize { get { return new Vector2(60, 60); } }
        public override Vector2 DefaultSize { get { return MinSize; } }

        private const string id = "ConcentratorNode";
        public override string GetID { get { return id; } }
        public override Type GetObjectType { get { return typeof(ConcentratorNode); } }

        [ConnectionKnob("", Direction.None, NodeSide.Left, StyleID = "StateStimulConnection", MaxConnectionCount = ConnectionCount.Single, NodeSidePos = 20)]
        public ConnectionKnob prevL;

        [ConnectionKnob("", Direction.None, NodeSide.Top, StyleID = "StateStimulConnection", MaxConnectionCount = ConnectionCount.Single, NodeSidePos = 20)]
        public ConnectionKnob prevT;

        [ConnectionKnob("", Direction.None, NodeSide.Bottom, StyleID = "StateStimulConnection", MaxConnectionCount = ConnectionCount.Single, NodeSidePos = 20)]
        public ConnectionKnob prevB;

        [ConnectionKnob("", Direction.Out, "StateConnection", NodeSide.Right, 20)]
        public ConnectionKnob next;

        protected override void OnCreate()
        {
            base.OnCreate();
        }

        protected override void DrawGUI()
        {
            GUILayout.BeginHorizontal();
            prevL.DisplayLayout();
            prevL.DrawKnob();
            prevT.DisplayLayout();
            prevT.DrawKnob();
            prevB.DisplayLayout();
            prevB.DrawKnob();
            next.DisplayLayout();
            GUILayout.EndHorizontal();
        }

        public StateBaseNode GetNextNode()
        {
            if (next.connected())
            {
                Node node = next.connections[0].body;
                if (node != null && node.GetID == "ConcentratorNode")
                    node = ((ConcentratorNode)node).GetNextNode();

                return (StateBaseNode)node;
            }
            else
                return null;
        }

        public bool ToAction()
        {
            if (!next.connected())
                return false;

            StateBaseNode node = (StateBaseNode)next.connections[0].body;

            return node != null && node.GetID == "ActionNode";
        }

        public bool ToState()
        {
            if (!next.connected())
                return false;

            StateBaseNode node = (StateBaseNode)next.connections[0].body;

            return node != null && node.GetID == "StateNode";
        }

        public ActionNode GetNextAction()
        {
            return (ActionNode)GetNextNode();
        }

        public StateNode GetNextState()
        {
            return (StateNode)GetNextNode();
        }
    }
}
