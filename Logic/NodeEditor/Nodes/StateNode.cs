﻿using NodeEditorFramework;
using System;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace Units.LogicFreeMode
{
    [Node(false, "Machine/State Node", new Type[] { typeof(MachineCanvas) })]
    public class StateNode : StateBaseNode
    {
        public override string Title { get { return "State Node"; } }
        public override Vector2 MinSize { get { return new Vector2(170, 80); } }

        private const string id = "StateNode";
        public override string GetID { get { return id; } }
        public override Type GetObjectType { get { return typeof(StateNode); } }

        [ConnectionKnob("", Direction.In, "StateConnection", NodeSide.Left, 50, MaxConnectionCount = ConnectionCount.Single)]
        public ConnectionKnob prevState;

        [SerializeField]
        public int stateId;

        [SerializeField]
        private List<StimulHolderOption> stimuls;

        private ConnectionKnobAttribute dynaCreationAttribute =
            new ConnectionKnobAttribute(
                "Action", Direction.Out, "StateStimulConnection", ConnectionCount.Single, NodeSide.Right, 50
            );

        protected override void OnCreate()
        {
            base.OnCreate();

            stimuls = new List<StimulHolderOption>();
            LoadAllStimuls();

            stateId = machineCanvas.statesCount;
        }

        protected override void DrawGUI()
        {
            if (dynamicConnectionPorts.Count != stimuls.Count)
            {
                while (dynamicConnectionPorts.Count > stimuls.Count)
                    DeleteConnectionPort(dynamicConnectionPorts.Count - 1);
                while (dynamicConnectionPorts.Count < stimuls.Count)
                    CreateConnectionKnob(dynaCreationAttribute);
            }

            EditorGUILayout.BeginVertical("Box");
            GUILayout.BeginHorizontal();
            prevState.DisplayLayout();
            EditorGUIUtility.labelWidth = 70;
            EditorGUILayout.LabelField("State id:   " + stateId.ToString());
            GUILayout.EndHorizontal();
            GUILayout.EndVertical();

            GUILayout.Space(2);


            GUILayout.BeginVertical("box");
            GUILayout.ExpandWidth(false);

            GUILayout.BeginHorizontal();
            GUILayout.Label("Stimuls", NodeEditorGUI.nodeLabelBoldCentered);

            GUILayout.EndHorizontal();
            GUILayout.Space(5);

            DrawStimuls();

            GUILayout.ExpandWidth(false);
            GUILayout.EndVertical();
        }

        [Serializable]
        class StimulHolderOption
        {
            public string stimulDisplay;		

            public StimulHolderOption(string stimulDisplay)
            {
                this.stimulDisplay = stimulDisplay;
            }
        }

        public ConnectionKnob GetStimulPort(string stimul)
        {
            int i = stimuls.FindIndex(x => x.stimulDisplay == stimul);
            if (i >= 0)
                return (ConnectionKnob)dynamicConnectionPorts[i];
            else
                return null;
        }

        private void RemoveLastStimul()
        {
            if (stimuls.Count > 1)
            {
                StimulHolderOption option = stimuls[stimuls.Count-1];
                stimuls.Remove(option);
                DeleteConnectionPort(dynamicConnectionPorts.Count - 1);
            }
        }

        private void DrawStimuls()
        {
            EditorGUILayout.BeginVertical();
            for (var i = 0; i < stimuls.Count; i++)
            {
                StimulHolderOption option = stimuls[i];
                GUILayout.BeginVertical();
                GUILayout.BeginHorizontal();
                EditorGUILayout.LabelField(i + ".", GUILayout.MaxWidth(15));
                option.stimulDisplay = EditorGUILayout.TextArea(option.stimulDisplay, GUILayout.MinWidth(80));
                ((ConnectionKnob)dynamicConnectionPorts[i]).SetPosition();

                GUILayout.EndHorizontal();
                GUILayout.EndVertical();
                GUILayout.Space(4);
            }
            GUILayout.EndVertical();
        }

        private void AddNewStimul(string name="Some stimul")
        {
            StimulHolderOption option = new StimulHolderOption(name);
            CreateConnectionKnob(dynaCreationAttribute);
            stimuls.Add(option);
        }

        //For Resolving the Type Mismatch Issue
        private void IssueEditorCallBacks()
        {
            NodeEditorCallbacks.IssueOnAddConnectionPort(dynamicConnectionPorts[stimuls.Count - 1]);
        }

        private void LoadAllStimuls()
        {
            string[] buf = machineCanvas.stateStimulDatabase.GetAll();

            for (int i = 0; i < buf.Length; i++)
            {
                AddNewStimul(buf[i]);
            }
        }

        public string[] GetAllStimuls()
        {
            string[] result = new string[stimuls.Count];

            for (int i=0; i< stimuls.Count; i++)
            {
                result[i] = stimuls[i].stimulDisplay;
            }

            return result;
        }

        public string[] GetStimuls()
        {
            string[] res = new string[stimuls.Count];

            for (int i = 0; i < res.Length; i++)
            {
                res[i] = stimuls[i].stimulDisplay;
            }

            return res;
        }


        private StateBaseNode GetStimulNext(string stimul)
        {
            int i = stimuls.FindIndex(x => x.stimulDisplay == stimul);

            if (i >= 0 && dynamicConnectionPorts[i].connected())
            {
                Node node = dynamicConnectionPorts[i].connections[0].body;
                if (node != null && node.GetID == "ConcentratorNode")
                    node = ((ConcentratorNode)node).GetNextNode();
                if (node != null && node.GetID == "ActionNode")
                    return (ActionNode)node;
                if (node != null && node.GetID == "StateNode")
                    return (StateNode)node;
                else
                    return null;
            }
            else
                return null;
        }

        public bool StimulToAction(string stimul)
        {
            StateBaseNode node = GetStimulNext(stimul);

            return node != null && node.GetID == "ActionNode";   
        }

        public bool StimulToState(string stimul)
        {
            StateBaseNode node = GetStimulNext(stimul);

            return node != null && node.GetID == "StateNode";
        }

        public ActionNode GetStimulAction(string stimul)
        {
            return (ActionNode)GetStimulNext(stimul);
        }

        public StateNode GetStimulState(string stimul)
        {
            return (StateNode)GetStimulNext(stimul);
        }
    }
}
