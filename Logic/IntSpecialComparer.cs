﻿using System.Collections.Generic;

namespace Units.LogicFreeMode
{
    // for dict<int, T>
    public class IntSpecialComparer : IEqualityComparer<int>
    {
        public bool Equals(int x, int y)
        {
            return x == y;
        }

        public int GetHashCode(int x)
        {
            return x.GetHashCode();
        }
    }
}
