﻿using System;
using UnityEngine;

namespace Units.LogicFreeMode
{
    public class StateMachine : IStateMachine
    {
        public string curMachineName { get; private set; }
        public int curStateId { get; private set; }

        public StateMachineStatus status { get; private set; }
        private bool enabled { get { return status == StateMachineStatus.Enabled; } }

        public IUnit unit { get; private set; }

        [Inject]
        public IStimulModel stimuls { get; protected set; }
        [Inject]
        public IStateBuilder builder { get; protected set; }
        [Inject]
        public IStateStimulDatabase stateStimulDatabase { get; protected set; }

        private int GetStimulId(PredefinedStateStimul stimul)
        {
            return stateStimulDatabase.GetId(stimul);
        }

        public StateMachine()
        {
            curMachineName = "";
            curStateId = -1;
            status = StateMachineStatus.NotInit;
        }

        public void Init(IUnit unit, string stateMachineName)
        {
            this.unit = unit;

            curMachineName = stateMachineName;
            stimuls.Init(this);
            builder.SetStateMachine(this, stateMachineName);

            status = StateMachineStatus.NotStarted;
        }

        public void StartMachine()
        {
            LogMessage(String.Format("stateMachine start"));
            status = StateMachineStatus.Enabled;
            ChangeState(0);
        }

        public void StopMachine()
        {
            LogMessage("stateMachine stop");
            status = StateMachineStatus.Stoped;
            StopAllReactions();
            ClearState();
            curMachineName = "";
        }

        public void ChangeState(int stateId)
        {
            if (enabled)
            {
                LogMessage(String.Format("state {0} to {1}", curStateId.ToString(), stateId.ToString()));
                builder.SetState(this, stateId);
                curStateId = stateId;
                DispatchStimul(GetStimulId(PredefinedStateStimul.StateChanged));
            }
        }

        public void DispatchStimul(int stimul)
        {
            if (enabled)
            {
                LogMessage(String.Format("Dispatch {0}", stateStimulDatabase[stimul]));
                stimuls.Dispatch(stimul);
            }
        }
        public void DispatchStimul(PredefinedStateStimul stimul)
        {
            DispatchStimul(GetStimulId(stimul));
        }

        public void ClearState()
        {
            stimuls.UnBindAll();
        }

        public void StopReaction(int stimul)
        {
            stimuls.StopReaction(stimul); 
        }

        public void StopAllReactions()
        {
            stimuls.StopAllReactions();
        }

        public void Pause()
        {
            status = StateMachineStatus.Paused;
            StopAllReactions();
        }

        public void Continue()
        {
            if (!String.IsNullOrEmpty(curMachineName) && status == StateMachineStatus.Paused)
            {
                status = StateMachineStatus.Enabled;
                ChangeState(curStateId);
            }
        }

        private void LogMessage(string messageString = "")
        {
            Debug.Log(String.Format("Unit {0}: {1}", unit.ID, messageString));
        }
    }
}
