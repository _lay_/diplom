﻿using Core;
using System.Collections.Generic;

namespace Units.LogicFreeMode
{
    public class StimulManager : IStimulManager
    {
        Dictionary<IStateMachine, List<IStimulListener>> listeners;

        public StimulManager()
        {
            listeners = new Dictionary<IStateMachine, List<IStimulListener>>();
        }

        private void Add(IStateMachine sm, IStimulListener listener)
        {
            if (!listeners.ContainsKey(sm))
            {
                listeners.Add(sm, new List<IStimulListener>());
            }

            listeners[sm].Add(listener);
        }

        public void AddListener(IStateMachine sm, Signal reasonSignal, int reactstimul)
        {
            IStimulListener listener = new SignalStimulListener(sm, reasonSignal, reactstimul, StimulListenerType.SIGNAL);

            Add(sm, listener);
        }

        public void AddListener(IStateMachine sm, ICondition condition, int reactstimul)
        {
            IStimulListener listener = new ConditionalStimulListener(sm, condition, reactstimul, StimulListenerType.CONDITION);

            Add(sm, listener);
        }

        public void AddListener(IStateMachine sm, Signal reasonSignal, ICondition condition, int reactstimul)
        {
            IStimulListener listener = new ConditionalToStimulListener(sm, reasonSignal, condition, reactstimul, StimulListenerType.CONDITION_TO_SIGNAL);

            Add(sm, listener);
        }

        public void RemoveListener(IStateMachine sm, Signal reasonSignal, int reactstimul)
        {
            IStimulListener listener = listeners[sm].Find(x =>
                ((SignalStimulListener)x).signal == reasonSignal &&
                ((SignalStimulListener)x).stimul == reactstimul
            );

            listener.Remove();
            listeners[sm].Remove(listener);
        }

        public void RemoveListener(IStateMachine sm, ICondition condition, int reactstimul)
        {
            IStimulListener listener = listeners[sm].Find(x =>
                ((ConditionalStimulListener)x).condition == condition &&
                ((ConditionalStimulListener)x).stimul == reactstimul
            );

            listener.Remove();
            listeners[sm].Remove(listener);
        }

        public void RemoveListener(IStateMachine sm, Signal reasonSignal, ICondition condition, int reactstimul)
        {
            IStimulListener listener = listeners[sm].Find(x =>
                ((ConditionalToStimulListener)x).signal == reasonSignal &&
                ((ConditionalToStimulListener)x).condition == condition &&
                ((ConditionalToStimulListener)x).stimul == reactstimul
            );

            listener.Remove();
            listeners[sm].Remove(listener);
        }
    }
}