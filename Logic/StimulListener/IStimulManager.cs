﻿using Core;

namespace Units.LogicFreeMode
{
    public interface IStimulManager
    {
        void AddListener(IStateMachine sm, Signal reasonSignal, int reactstimul);
        void AddListener(IStateMachine sm, ICondition condition, int reactstimul);
        void AddListener(IStateMachine sm, Signal reasonSignal, ICondition condition, int reactstimul);

        void RemoveListener(IStateMachine sm, Signal reasonSignal, int reactstimul);
        void RemoveListener(IStateMachine sm, ICondition condition, int reactstimul);
        void RemoveListener(IStateMachine sm, Signal reasonSignal, ICondition condition, int reactstimul);
    }
}