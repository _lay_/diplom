﻿using Core;

namespace Units.LogicFreeMode
{
    public class SignalStimulListener : IStimulListener
    {
        private IStateMachine sm { get; set; }

        public int stimul { get; private set; }
        public Signal signal { get; private set; }
        public StimulListenerType type { get; private set; }

        private SignalStimulListener() { }
        public SignalStimulListener(IStateMachine sm, Signal signal, int stimul, StimulListenerType type)
        {
            this.sm = sm;

            this.stimul = stimul;
            this.signal = signal;
            this.type = type;

            signal.AddListener(DispatchStimul);
        }

        void DispatchStimul()
        {
            sm.stimuls.Dispatch(stimul);
        }

        public void Remove()
        {
            signal.RemoveListener(DispatchStimul);
        }
    }
}