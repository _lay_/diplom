﻿
namespace Units.LogicFreeMode
{
    public interface IStimulListener
    {
        StimulListenerType type { get; }

        void Remove();
    }
}