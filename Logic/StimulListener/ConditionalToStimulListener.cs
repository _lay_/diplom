﻿using Core;
using Core.Unity;
using UnityEngine;

namespace Units.LogicFreeMode
{
    public class ConditionalToStimulListener : IStimulListener
    {
        private IStateMachine sm { get; set; }

        public int stimul { get; private set; }
        public Signal signal { get; private set; }
        public ICondition condition { get; private set; }
        public StimulListenerType type { get; private set; }

        [Inject]
        public ICoroutineCreator coroutineCreator { get; protected set; }
        private Coroutine coroutine { get; set; }

        private ConditionalToStimulListener() { }
        public ConditionalToStimulListener(IStateMachine sm, Signal signal, ICondition condition, int stimul, StimulListenerType type)
        {
            this.sm = sm;

            coroutine = null;
            this.stimul = stimul;
            this.signal = signal;
            this.condition = condition;
            this.type = type;

            signal.AddListener(ConditionalSignalListener);
        }
        void ConditionalSignalListener()
        {
            if (condition.Check())
            {
                sm.stimuls.Dispatch(stimul);
            }
        }

        public void Remove()
        {
            signal.RemoveListener(ConditionalSignalListener);
        }
    }
}