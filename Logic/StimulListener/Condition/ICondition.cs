﻿
namespace Units.LogicFreeMode
{
    public interface ICondition
    {
        bool Check();
    }
}