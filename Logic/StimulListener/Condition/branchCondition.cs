﻿using System;

namespace Units.LogicFreeMode
{
    public class BranchCondition : ICondition
    {
        private ICondition left { get; set; }
        private ICondition right { get; set; }

        private BranchConditionType type { get; set; }

        private BranchCondition() { }
        public BranchCondition(ICondition left, ICondition right, BranchConditionType type)
        {
            this.left = left;
            this.right = right;
            this.type = type;
        }

        public bool Check()
        {
            bool l = left.Check();
            bool r = right.Check();

            switch (type)
            {
                case BranchConditionType.AND:
                    return l & r;
                case BranchConditionType.OR:
                    return l | r;
                default:
                    return false;
            }
        }
    }
}