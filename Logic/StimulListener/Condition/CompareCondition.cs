﻿using System;

namespace Units.LogicFreeMode
{
    public abstract class CompareCondition<T> : ICondition
    {
        protected T left { get; set; }
        protected T right { get; set; }

        private CompareCondition() { }
        public CompareCondition(T left, T right)
        {
            this.left = left;
            this.right = right;
        }

        public abstract bool Check();
    }

    public class EqualsCondition<T> : CompareCondition<T>
    {
        public EqualsCondition(T left, T right) : base(left, right) { }

        public override bool Check()
        {
            return left.Equals(right);
        }
    }

    public class NotEqualsCondition<T> : CompareCondition<T>
    {
        public NotEqualsCondition(T left, T right) : base(left, right) { }

        public override bool Check()
        {
            return !left.Equals(right);
        }
    }
}