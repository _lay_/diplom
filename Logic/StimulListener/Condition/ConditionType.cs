﻿
namespace Units.LogicFreeMode
{
    public enum BranchConditionType { AND, OR }

    public enum LeafConditionType { EQ, NEQ, GREATER, LESS }
}