﻿using Core.Unity;
using System.Collections;
using UnityEngine;

namespace Units.LogicFreeMode
{
    public class ConditionalStimulListener : IStimulListener
    {
        private IStateMachine sm { get; set; }

        public int stimul { get; private set; }
        public ICondition condition { get; private set; }
        public StimulListenerType type { get; private set; }

        [Inject]
        public ICoroutineCreator coroutineCreator { get; protected set; }
        private Coroutine coroutine { get; set; } 

        private ConditionalStimulListener() { }
        public ConditionalStimulListener(IStateMachine sm, ICondition condition, int stimul, StimulListenerType type)
        {
            this.sm = sm;

            coroutine = null;
            this.stimul = stimul;
            this.condition = condition;
            this.type = type;

            coroutine = coroutineCreator.StartCoroutine(Coroutine());
        }

        IEnumerator Coroutine()
        {
            while (true)
            {
                yield return null;

                if (condition.Check())
                {
                    sm.stimuls.Dispatch(stimul);
                }
            }
        }

        public void Remove()
        {
            coroutineCreator.StopCoroutine(coroutine);
        }
    }
}