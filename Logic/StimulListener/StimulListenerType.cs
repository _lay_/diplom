﻿
namespace Units.LogicFreeMode
{
    public enum StimulListenerType { SIGNAL, CONDITION, CONDITION_TO_SIGNAL }
}