﻿using UnityEditor;
using UnityEngine;

namespace Units.LogicFreeMode
{
    [CustomEditor(typeof(StateStimulDatabase))]
    class StateStimulDatabaseInspector : Editor
    {
        private StateStimulDatabase thisDatabase;

        private void OnEnable()
        {
            thisDatabase = (StateStimulDatabase)target;
        }

        public override void OnInspectorGUI()
        {
            StimulList();
            TopButtons();

            base.DrawDefaultInspector();
        }

        private void TopButtons()
        {
            GUILayout.BeginHorizontal();
            //AddNewStimulButton();
            GUILayout.EndHorizontal();
        }

        private void StimulList()
        {
            EditorGUILayout.LabelField("Predefined");
            GUILayout.BeginVertical("Box");

            if (thisDatabase.count == 0)
            {
                GUILayout.Label("empty");
            }

            for (int i=0; i< thisDatabase.countPredefined; i++)
            {
                GUILayout.BeginHorizontal();
                EditorGUILayout.LabelField(i.ToString() + ": ", thisDatabase[i]);
                GUILayout.EndHorizontal();
            }

            GUILayout.EndVertical();
        }

        private void AddNewStimulButton()
        {
            if (GUILayout.Button(
                "Add new"
            ))
            {

            }
        }
    }
}
