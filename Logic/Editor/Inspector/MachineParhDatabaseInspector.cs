﻿using UnityEditor;
using UnityEngine;

namespace Units.LogicFreeMode
{
    [CustomEditor(typeof(StateMachinePathDatabase))]
    class MachineParhDatabaseInspector : Editor
    {
        private StateMachinePathDatabase thisDatabase;

        public override void OnInspectorGUI()
        {
            if (thisDatabase == null)
                thisDatabase = (StateMachinePathDatabase)target;

            TopButtons();
            DrawData();
        }

        private void TopButtons()
        {
            EditorGUILayout.BeginHorizontal();
            AddNewButton();
            //OpenStateGraphButton();
            EditorGUILayout.EndHorizontal();
        }

        private void AddNewButton()
        {
            if (GUILayout.Button(
                "Add new"
            ))
            {
                thisDatabase.Add("New_" + thisDatabase.count);
            }
        }
        private void OpenStateGraphButton()
        {
            if (GUILayout.Button(
                "Open state graph"
            ))
            {
                
            }
        }

        private void DrawData()
        {
            var data = thisDatabase.GetAllItems();
            GUILayout.Label("StateMachines:");
            EditorGUILayout.BeginVertical("Box");

            if (data.Length == 0)
            {
                GUILayout.Label("empty");
            }

            for (int i = 0; i < data.Length; i++)
            {
                var item = data[i];
                
                EditorGUILayout.BeginHorizontal();

                EditorGUILayout.LabelField(i.ToString() + ": ", new GUILayoutOption[] { GUILayout.Width(15) });

                EditorGUILayout.BeginVertical();

                EditorGUILayout.BeginHorizontal();
                EditorGUILayout.LabelField("name:", new GUILayoutOption[] { GUILayout.Width(50) });
                string bufName = EditorGUILayout.TextField(item.Key);
                EditorGUILayout.EndHorizontal();

                EditorGUILayout.BeginHorizontal();
                string bufPath = item.Value;
                EditorGUILayout.LabelField("path:", new GUILayoutOption[] { GUILayout.Width(50) });
                EditorGUILayout.TextField(bufPath);
                if (GUILayout.Button("select", new GUILayoutOption[] { GUILayout.Width(50) }))
                {
                    bufPath = UnityEditor.EditorUtility.OpenFilePanel("Select machine file", item.Value, "json");
                    if (bufPath.Contains(Application.dataPath))
                    {
                        string buf = Application.dataPath;
                        buf = buf.Remove(buf.Length - 6);
                        bufPath = bufPath.Replace(buf, "");
                    }     
                }
                EditorGUILayout.EndHorizontal();

                EditorGUILayout.EndVertical();

                if (GUILayout.Button("-", new GUILayoutOption[] { GUILayout.Width(25) }))
                {
                    thisDatabase.Remove(item.Key);
                    i--;
                    EditorUtility.SetDirty(thisDatabase);
                }

                EditorGUILayout.EndHorizontal();

                if (bufName != item.Key)
                {
                    thisDatabase.Rename(item.Key, bufName);
                }

                if (bufPath != item.Value)
                {
                    thisDatabase.SetPath(item.Key, bufPath);
                }
            }

            EditorGUILayout.EndVertical();
        }
    }
}
