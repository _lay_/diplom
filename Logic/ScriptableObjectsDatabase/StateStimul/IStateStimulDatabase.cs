﻿
namespace Units.LogicFreeMode
{
    public interface IStateStimulDatabase : IStringIdDatabase<PredefinedStateStimul>
    {
        string this[PredefinedStateStimul stimul] { get; }
        string this[int id] { get; }
        int count { get; }
        int countPredefined { get; }
        int countCustom { get; }

        bool isPredefined(string stimul);

        string[] GetStateStimulNames();
        int[] GetStateStimulIDs();

        int GetId(string stimul);
        int GetId(PredefinedStateStimul stimul);
    }
}
