﻿using Serialization;
using System;
using UnityEngine;

namespace Units.LogicFreeMode
{
    [CreateAssetMenu(fileName = "StateStimulDatabase", menuName = "LogicComponent/StateStimulDatabase")]
    public class StateStimulDatabase : StringIdDatabase<PredefinedStateStimul>, IStateStimulDatabase
    {
        [Inject]
        public IJsonInjector JsonInjector { get; protected set; }

        public string this[PredefinedStateStimul stimul] { get { return stimul.Name(); } }
        public string this[int id] { get { return id < predefined.Length ? predefined[id] : custom[id - predefined.Length]; } }
        public int count { get { return countPredefined + countCustom; } }

        public int countPredefined { get { return predefined.Length; } }
        public int countCustom { get { return custom.Length; } }

        public bool isPredefined(string stimul)
        {
            return Enum.IsDefined(typeof(PredefinedStateStimul), stimul);
        }

        public string[] GetStateStimulNames()
        {
            return GetAll();
        }

        public int[] GetStateStimulIDs()
        {
            int[] res = new int[count];

            for (int i = 0; i < count; i++)
            {
                res[i] = i;
            }

            return res;
        }

        // id = index in prededied+custom
        public int GetId(string stimul)
        {
            int i = Array.IndexOf<string>(predefined, stimul);

            return i < 0 ? Array.IndexOf<string>(custom, stimul) : i;
        }
        public int GetId(PredefinedStateStimul stimul)
        {
            return CastToInt(stimul);
        }

        protected override int CastToInt(PredefinedStateStimul value)
        {
            return (int)value;
        }

        protected override string ToString(PredefinedStateStimul value)
        {
            return value.Name();
        }

        protected override bool UseCustom()
        {
            return true;
        }

        protected override bool DefaultIdEditEnabled()
        {
            return false;
        }
    }
}