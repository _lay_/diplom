﻿using System;

namespace Units.LogicFreeMode
{
    public enum PredefinedStateStimul
    {
        StateChanged,
        StateTick,
        WalkCompleteStimul,
        ObjectTakedStimul,
        ObjectUsedStimul,
        MoveCompleteStimul,
        WaitCompleteStimul,
        RouteEndStimul
    }

    public static class PredefinedStateStimulExtensions
    {
        private static string[] names = Enum.GetNames(typeof(PredefinedStateStimul));

        public static string Name(this PredefinedStateStimul type)
        {
            return names[(int)type];
        }
    }
}