﻿
using System.Collections.Generic;

namespace Units.LogicFreeMode
{
    public interface IStateMachinePathDatabase
    {
        string this[string machineName] { get; }

        int count { get; }

        KeyValuePair<string, string>[] GetAllItems();
        string[] GetStateMachineNames();
        string[] GetStateMachinePaths();
        string GetName(string path);

        bool ContainsName(string name);
        bool ContainsPath(string path);

        void Add(string name);
        void SetPath(string name, string path);
        void Rename(string oldName, string newName);
        void Remove(string name);
    }
}
