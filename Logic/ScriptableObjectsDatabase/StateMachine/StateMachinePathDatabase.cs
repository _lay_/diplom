﻿using Core.Unity;
using System.Collections.Generic;
using UnityEngine;

namespace Units.LogicFreeMode
{
    public class StringDictComparer : EqualityComparer<string>
    {
        public override bool Equals(string x, string y)
        {
            return x == y;
        }

        public override int GetHashCode(string obj)
        {
            return obj.GetHashCode();
        }
    }

    [CreateAssetMenu(fileName = "StateMachinePathDatabase", menuName = "LogicComponent/StateMachinePathDatabase")]
    public class StateMachinePathDatabase : ScriptableObject, IStateMachinePathDatabase
    {
        [System.Serializable]
        class DataDict : SerializableDictionary<string, string>
        {
            public DataDict() : base(new StringDictComparer()) { }
        }

        [SerializeField]
        private DataDict data;

        public string this[string machineName] => data[machineName];
        public int count => data.Count;

        public KeyValuePair<string, string>[] GetAllItems()
        {
            List<KeyValuePair<string, string>> buf = new List<KeyValuePair<string, string>>();
            foreach (var item in data)
            {
                buf.Add(new KeyValuePair<string, string>(item.Key, item.Value));
            }
            return buf.ToArray();
        }

        public string[] GetStateMachineNames() 
        {
            string[] buf = new string[count];
            data.Keys.CopyTo(buf, 0);
            return buf;
        }

        public string[] GetStateMachinePaths()
        {
            string[] buf = new string[count];
            data.Values.CopyTo(buf, 0);
            return buf;
        }

        public string GetName(string path)
        {
            foreach (var item in data)
            {
                if (item.Value == path)
                    return item.Key;

            }
            return "";
        }

        public bool ContainsName(string name)
        {
            return data.ContainsKey(name);
        }

        public bool ContainsPath(string path)
        {
            return data.ContainsValue(path);
        }

        public void Add(string name)
        {
            data.Add(name, "");
            Save();
        }

        public void SetPath(string name, string path)
        {
            data[name] = path;
            Save();
        }

        public void Rename(string oldName, string newName)
        {
            string path = data[oldName];
            data.Remove(oldName);
            data.Add(newName, path);
            Save();
        }

        public void Remove(string name)
        {
            data.Remove(name);
            Save();
        }

        private void Save()
        {
#if UNITY_EDITOR
            UnityEditor.EditorUtility.SetDirty(this);
#endif
        }
    }
}