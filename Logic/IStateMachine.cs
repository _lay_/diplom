﻿
namespace Units.LogicFreeMode
{
    public interface IStateMachine
    {
        string curMachineName { get; }
        int curStateId { get; }

        IUnit unit { get; }

        IStimulModel stimuls { get; }

        void ClearState();

        void Init(IUnit unit, string curMachineName);
        void StartMachine();
        void StopMachine();
        void ChangeState(int stateId);
        void DispatchStimul(int stimul);
        void DispatchStimul(PredefinedStateStimul stimul);

        void StopReaction(int stimul);
        void StopAllReactions();

        void Pause();
        void Continue();
    }
}
