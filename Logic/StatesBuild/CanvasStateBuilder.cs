﻿using System;
using System.Collections.Generic;
using strange.extensions.command.api;
using UnityEditor;

namespace Units.LogicFreeMode
{
    public class CanvasStateBuilder : IStateBuilder
    {
        private StateMachineItem stateMachine { get; set; }

        [Inject]
        public ICommandBinder binder { get; protected set; }
        [Inject]
        public IGameLogger logger { get; protected set; }

        [Inject]
        public IStateStimulDatabase stateStimulDatabase { get; protected set; }
        [Inject]
        public IStateMachinePathDatabase stateMachinePathDatabase { get; protected set; }
        [Inject]
        public JsonStateDataProvider dataprovider { get; protected set; }

        public void SetStateMachine(IStateMachine sm, string smName)
        {
            ReLoadStates(smName);

            SetStimuls(sm);
            SetState(sm, 0);
        }

        public void SetState(IStateMachine sm, int stateId)
        {
            StateItem state = stateMachine.GetState(stateId);
            if (state == null)
            {
                throw new KeyNotFoundException("state " + stateId + " not found!");
            }

            sm.ClearState();

            List<Type> types = new List<Type>();

            foreach (var item in state.reactions)
            {
                types.Clear();

                if (!stateStimulDatabase.Contains(item.stimul))
                {
                    throw new KeyNotFoundException("Stimul " + item.stimul + " not found!");
                }

                int stimulId = stateStimulDatabase.GetId(item.stimul);

                foreach (var reaction in item.reactions)
                {
                    Type type = Type.GetType(reaction.reactionType);
                    types.Add(type);

                    if (reaction.parameters != null)
                        foreach (var param in reaction.parameters)
                            sm.stimuls.GetData(stimulId).SetValue(param.name, param.type, param.value);
                }
                sm.stimuls.Bind(stimulId, types.ToArray());
            }

        }

        private void SetStimuls(IStateMachine sm)
        {
            sm.stimuls.AddRange(stateStimulDatabase.GetStateStimulIDs());
        }


        private void ReLoadStates(string smName)
        {
            if (!stateMachinePathDatabase.ContainsName(smName))
            {
                throw new ArgumentOutOfRangeException("state machine name", "can't found machine name=" + smName);
            }

            string jsonPath = stateMachinePathDatabase[smName];
            stateMachine = dataprovider.Load(jsonPath);
        }
    }
}
