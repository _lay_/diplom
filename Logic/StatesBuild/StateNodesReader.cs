﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Units.LogicFreeMode
{
    public class StateNodesReader
    {
        public static StateMachineItem ReadStateCanvas(MachineCanvas canvas)
        {
            StateRootNode root = canvas.GetStateRootNode();

            if (canvas.GetStatesCount() == 0)
                return new StateMachineItem(new StateItem[0]);


            StateNode[] nodes = canvas.GetStates();
            StateItem[] states = new StateItem[nodes.Length];

            for (int i = 0; i < states.Length; i++)
            {
                states[i] = ReadStateNode(nodes[i]);
            }

            return new StateMachineItem(states);
        }

        public static StateItem ReadStateNode(StateNode node)
        {
            List<StimulReactionItem> stimulsReaction = new List<StimulReactionItem>();

            string[] stimuls = node.GetAllStimuls();

            foreach (string stimul in stimuls)
            {
                List<ReactionItem> reactions = new List<ReactionItem>();

                if (node.StimulToState(stimul))
                {
                    int id = node.GetStimulState(stimul).stateId;
                    reactions.Add(InitChangeStateReactionItem(id));
                }
                else if (node.StimulToAction(stimul))
                {
                    ActionNode action = node.GetStimulAction(stimul);
                    reactions.Add(ReadActionNode(action));
                    while (action.ActionToAction())
                    {
                        action = action.GetNextAction();
                        reactions.Add(ReadActionNode(action));
                    }

                    if (action.ActionToState())
                    {
                        int id = action.GetNextState().stateId;
                        reactions.Add(InitChangeStateReactionItem(id));
                    }
                }

                stimulsReaction.Add(new StimulReactionItem(stimul, reactions.ToArray()));
            }

            return new StateItem(node.stateId, stimulsReaction.ToArray(), node.position);
        }

        public static ReactionItem ReadActionNode(ActionNode node)
        {
            Type t = node.GetActionType();
            if (t == null)
            {
                throw new MissingFieldException("action node have not reactionType!");
            }

            if (node.GetAllParameters() == null)
                return new ReactionItem(t.ToString(), new ParameterItem[0], node.position);
            else
            {
                var prs = node.GetAllParameters();
                var tmp = new ParameterItem[prs.Length];
                int i = 0;
                foreach (var p in prs)
                {
                    Type type = Type.GetType(p.type);

                    tmp[i] = new ParameterItem(p.name, type.ToString(), p.value);
                    i++;
                }
                return new ReactionItem(t.ToString(), tmp, node.position);
            }
        }

        public static ReactionItem InitChangeStateReactionItem(int id)
        {
            ParameterItem val = new ParameterItem("newStateId", typeof(int).ToString(), id);
            return new ReactionItem(
                        typeof(ChangeStateCommandReaction).ToString(),
                        new ParameterItem[] { val },
                        Vector2.zero
                        );
        }
    }
}
