﻿
namespace Units.LogicFreeMode
{
    public interface IStateBuilder
    {
        void SetStateMachine(IStateMachine sm, string smName);
        void SetState(IStateMachine sm, int stateId);
    }
}
