﻿using System.IO;
using System.Runtime.Serialization.Json;

namespace Units.LogicFreeMode
{
    public class JsonStateDataProvider
    {
        DataContractJsonSerializer jsonFormatter = new DataContractJsonSerializer(typeof(StateMachineItem));

        public void Save(StateMachineItem stateMachine, string filePath)
        {
            using (FileStream fs = new FileStream(filePath, FileMode.Create))
            {
                jsonFormatter.WriteObject(fs, stateMachine);
            }
        }

        public StateMachineItem Load(string filePath)
        {
            StateMachineItem newSM;

            using (FileStream fs = new FileStream(filePath, FileMode.OpenOrCreate))
            {
                newSM = (StateMachineItem)jsonFormatter.ReadObject(fs);
            }

            return newSM;
        }
    }
}
