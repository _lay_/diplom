﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using UnityEngine;

namespace Units.LogicFreeMode
{
    [DataContract]
    public class ReactionItem
    {
        [DataMember]
        public string reactionType { get; set; }
        [DataMember]
        public ParameterItem[] parameters { get; set; }
        [DataMember]
        public Vector2 pos { get; set; }

        public ReactionItem(string reactionType, ParameterItem[] parameters, Vector2 pos)
        {
            this.reactionType = reactionType;
            this.parameters = parameters;
            if (pos == null)
                this.pos = Vector2.zero;
            else
                this.pos = pos;
        }
    }
}
