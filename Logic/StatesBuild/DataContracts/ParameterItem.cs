﻿using System.Runtime.Serialization;

namespace Units.LogicFreeMode
{
    [DataContract]
    public class ParameterItem
    {
        [DataMember]
        public string name { get; set; }
        [DataMember]
        public string type { get; set; }
        [DataMember]
        public object value { get; set; }

        public ParameterItem(string name, string type, object value)
        {
            this.name = name;
            this.type = type;
            this.value = value;
        }
    }
}
