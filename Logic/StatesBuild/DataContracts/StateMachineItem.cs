﻿using System;
using System.Runtime.Serialization;

namespace Units.LogicFreeMode
{
    [DataContract]
    public class StateMachineItem
    {
        [DataMember]
        public StateItem[] states { get; set; }

        public StateMachineItem()
        {
            this.states = new StateItem[0];
        }
        public StateMachineItem(StateItem[] states)
        {
            this.states = states;
        }

        public StateItem GetState(int id)
        {
            return Array.Find(states, x => x.id == id);
        }

        public bool isEmpty { get { return states.Length == 0; } }
    }
}
