﻿using System.Runtime.Serialization;

namespace Units.LogicFreeMode
{
    [DataContract]
    public class StimulReactionItem
    {
        [DataMember]
        public string stimul { get; set; }
        [DataMember]
        public ReactionItem[] reactions { get; set; }

        public StimulReactionItem(string stimul, ReactionItem[] reactions)
        {
            this.stimul = stimul;
            this.reactions = reactions;
        }
    }
}
