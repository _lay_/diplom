﻿using System.Runtime.Serialization;
using UnityEngine;

namespace Units.LogicFreeMode
{
    [DataContract]
    public class StateItem
    {
        [DataMember]
        public int id { get; set; }
        [DataMember]
        public StimulReactionItem[] reactions { get; set; }

        [DataMember]
        public Vector2 pos { get; set; }

        public StateItem(int id, StimulReactionItem[] reactions, Vector2 pos)
        {
            this.id = id;
            this.reactions = reactions;
            if (pos == null)
                this.pos = Vector2.zero;
            else
                this.pos = pos;
        }
    }
}
