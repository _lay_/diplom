﻿
namespace Units.LogicFreeMode
{
    public enum StateMachineStatus
    {
        NotInit,
        NotStarted,
        Enabled,
        Paused,
        Stoped
    }
}