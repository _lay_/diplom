﻿using System.Collections.Generic;
using Tiles;

namespace Units.LogicFreeMode
{
    public class Route : IRoute
    {
        List<CellCoordinates> route = new List<CellCoordinates>();

        int i = 0;

        public CellCoordinates current { get { return route[i]; } private set { route[i] = value; } }
        public bool isLast { get { return i + 1 == count; } }
        public bool isFirst { get { return i == 0; } }
        public int count { get { return route.Count; } }

        public CellCoordinates GoNext()
        {
            if (isLast)
            {
                i = 0;
            }
            else
            {
                i++;
            }
            return current;
        }
        public CellCoordinates GoPrev()
        {
            if (i != 0)
                i--;
            return current;
        }
        public CellCoordinates GoFirst()
        {
            i = 0;
            return current;
        }

        public void WriteNewRoute(CellCoordinates[] newRoute)
        {
            route.Clear();
            i = 0;
            route.AddRange(newRoute);
        }
    }
}
