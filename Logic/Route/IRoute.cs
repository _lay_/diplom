﻿using Tiles;

namespace Units.LogicFreeMode
{
    public interface IRoute
    {
        CellCoordinates current { get; }
        bool isLast { get; }
        bool isFirst { get; }
        int count { get; }

        CellCoordinates GoNext();
        CellCoordinates GoPrev();
        CellCoordinates GoFirst();

        void WriteNewRoute(CellCoordinates[] newRoute);
    }
}
