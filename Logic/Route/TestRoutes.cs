﻿using Tiles;
using UnityEngine;

namespace Units.LogicFreeMode
{
     public class TestRoutes
     {
        static CellCoordinates Point(int x, int y)
        {
            return new CellCoordinates(x, y);
        }

        const int a = 3;

        public static CellCoordinates[] GetTestRoute(CellCoordinates pos)
        {
            CellCoordinates[] points = new CellCoordinates[] {
                Point(pos.X, pos.Y),
                Point(pos.X+a, pos.Y+a), Point(pos.X-a, pos.Y+a),
                Point(pos.X-a, pos.Y-a), Point(pos.X+a, pos.Y-a),
                Point(pos.X, pos.Y) };

            return points;
        }
     }
}
