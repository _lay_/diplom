﻿using System;

namespace Units.LogicFreeMode
{
    public interface IStimulModel
    {
        void Init(IStateMachine sm);
        void Bind(int stimul, Type commandReactionType);
        void Bind(int stimul, Type[] commandReactionTypes);
        StateData GetData(int stimul);
        void UnBind(int stimul);
        void UnBindAll();
        void Dispatch(int stimul);
        bool Contains(int stimul);
        void Add(int stimul);
        void AddRange(int[] stimul);
        void Remove(int stimul);
        void RemoveAll();

        void StopReaction(int stimul);
        void StopAllReactions();

        int[] GetAll();
    }
}
