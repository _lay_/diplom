﻿using Core;
using strange.extensions.command.api;
using System;
using System.Collections.Generic;

namespace Units.LogicFreeMode
{
    public class StimulModel : IStimulModel
    {
        private struct ModelObject
        {
            public Signal<StateData> signal;
            public StateData data;

            public ModelObject(Signal<StateData> signal, StateData data)
            {
                this.signal = signal;
                this.data = data;
            }

            public void Dispatch()
            {
                signal.Dispatch(data);
            }
        }

        private Dictionary<int, ModelObject> stimuls;
        private IStateMachine sm;

        [Inject]
        public ICommandBinder CommandBinder { get; protected set; }
        [Inject]
        public IStateStimulDatabase stateStimulDatabase { get; protected set; }

        public StimulModel()
        {
            stimuls = new Dictionary<int, ModelObject>(new IntSpecialComparer());
        }


        public void Init(IStateMachine sm)
        {
            this.sm = sm;
            stimuls.Clear();
        }

        public void Bind(int stimul, Type commandReactionType)
        {
            CommandBinder.Bind(stimuls[stimul].signal).To(commandReactionType);
        }

        public void Bind(int stimul, Type[] commandReactionTypes)
        {
            ICommandBinding bind = CommandBinder.Bind(stimuls[stimul].signal).InSequence();
            for (int i=0; i< commandReactionTypes.Length; i++)
            {
                bind.To(commandReactionTypes[i]);
            }
        }

        public StateData GetData(int stimul)
        {
            return stimuls[stimul].data;
        }

        public void UnBind(int stimul)
        {
            ModelObject obj = stimuls[stimul];
            CommandBinder.Unbind(obj.signal);
            obj.data.ClearValues();
        }

        public void UnBindAll()
        {
            foreach (int stimul in stimuls.Keys)
            {
                UnBind(stimul);
            }
        }

        public void Dispatch(int stimul)
        {
            if (Contains(stimul))
            {
                stimuls[stimul].Dispatch();
            }
            else
                StimulNotFoundThrow(stateStimulDatabase[stimul]);
        }

        public void Add(int stimul)
        {
            stimuls.Add(stimul, new ModelObject(new Signal<StateData>(), new StateData(sm)));
        }

        public void AddRange(int[] stimuls)
        {
            foreach (int stimul in stimuls)
                Add(stimul);
        }

        public bool Contains(int stimul)
        {
            return stimuls.ContainsKey(stimul);
        }

        public int[] GetAll()
        {
            int[] res = new int[stimuls.Count];
            stimuls.Keys.CopyTo(res, 0);
            return res;
        }

        public void StopReaction(int stimul)
        {
            if (Contains(stimul))
                CommandBinder.Stop(stimuls[stimul].signal);
            else
                StimulNotFoundThrow(stateStimulDatabase[stimul]);
        }

        public void StopAllReactions()
        {
            foreach (int s in stimuls.Keys)
            {
                StopReaction(s);
            }
        }

        public void RemoveAll()
        { 
            stimuls.Clear(); 
        }

        public void Remove(int stimul)
        {
            if (Contains(stimul))
            {
                UnBind(stimul);
                stimuls.Remove(stimul);
            }
            else
                StimulNotFoundThrow(stateStimulDatabase[stimul]);
        }

        private void StimulNotFoundThrow(string stimulName)
        {
            throw new KeyNotFoundException("Stimul " + stimulName + " not found!");
        }
    }
}
