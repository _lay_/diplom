﻿using UnityEngine;
using Units.LogicFreeMode;

namespace Units.Components
{
    public class LogicFreeModeComponent : ILogicFreeModeComponent
    {
        [Inject]
        public IStateMachine sm { get; protected set; }

        [Inject]
        public IRoute route { get; protected set; }

        bool isEnemy { get; set; }

        public void Init(IUnit unit, string stateMachineName)
        {
            route.WriteNewRoute(TestRoutes.GetTestRoute(unit.MotionComponent.CurrentCellCoordinates));

            isEnemy = (unit.Fraction == Managers.Fraction.Enemy);

            sm.Init(unit, stateMachineName);
        }

        public void StartStateMachine()
        {
            if (isEnemy)
            {
                sm.StartMachine();
            }
        }

        public void StopStateMachine()
        {
            if (isEnemy)
            {
                sm.StopMachine();
            }
        }

        public void ContinueStateMachine()
        {
            if (isEnemy)
            {
                Debug.Log("Continue");
                sm.Continue();
            }
        }
        public void PauseStateMachine()
        {
            if (isEnemy)
            {
                Debug.Log("Pause");
                sm.Pause();
            }
        }

        public void DispatchStimul(int stimul)
        {
            if (isEnemy)
            {
                sm.DispatchStimul(stimul);
            }
        }
        public void DispatchStimul(PredefinedStateStimul stimul)
        {
            if (isEnemy)
            {
                sm.DispatchStimul(stimul);
            }
        }
    }
}
